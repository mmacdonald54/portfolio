<?php

//Start cookie session
session_start();
//loads the Swiper that displays the books
require "levelDynamic.php";
$langID = $_SESSION['lang'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>NDA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link type="text/css" rel="stylesheet" href="stylesheets/addtohomescreen.css"/>
    <script type="text/javascript" src="scripts/addtohomescreen.js"></script>
    <script>
        addToHomescreen();
    </script>
    <link rel="apple-touch-icon" href="touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="css-menu/demo.css" />
    <link type="text/css" rel="stylesheet" href="javascript-menu/css/jquery.mmenu.all.css" />
    <link rel="stylesheet" type="text/css" href="css-menu/jquery.dialogbox.css">
    <link rel="stylesheet" type="text/css" href="css-menu/menuStyle.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/scripts.js"></script>

    <link href="stylesheets/style.css" rel="stylesheet">


    <script type="text/javascript" src="javascript-menu/js/jquery.mmenu.min.all.js"></script>
    <script type="text/javascript">
        $(function () {
            $('nav#menu').mmenu({
                extensions: ['effect-slide', 'pageshadow'],
                header: true,
                searchfield: true,
                counters: true
            });
        });
    </script>
    <script>
        // ===== Scroll to Top ====
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
    </script>

</head>
<body id="<?php echo $langID; ?>">
<a href="#page" id="return-to-top"><i class="icon-chevron-up"></i></a>

<!--this include is on every page and includes the code for the menu-->
<?php require ("actionPages/include.php");?>
<div class="container">
    <div class="col-xs-12 text-center">
        <a href="#Level A"><img src="images/siteMedia/levelA.png" height="40" width="40" class="img-circle"/></a>
        <a href="#Level B"><img src="images/siteMedia/levelB.png" height="40" width="40" class="img-circle"/></a>
        <a href="#Level C"><img src="images/siteMedia/levelC.png" height="40" width="40" class="img-circle"/></a>
        <a href="#Level D"><img src="images/siteMedia/levelD.png" height="40" width="40" class="img-circle"/></a>
        <a href="#Level E"><img src="images/siteMedia/levelE.png" height="40" width="40" class="img-circle"/></a>
        <a href="#Level F"><img src="images/siteMedia/levelF.png" height="40" width="40" class="img-circle"/></a>
        <a href="#Level G"><img src="images/siteMedia/levelG.png" height="40" width="40" class="img-circle"/></a>
    </div>
    <div class="col-xs-12 text-center">
        <form name="rules">
            <label class="checkbox-inline"><input type="checkbox" id="1">Long AI</label>
            <label class="checkbox-inline"><input type="checkbox" id="2">Long AY</label>
            <label class="checkbox-inline"><input type="checkbox" id="3">Long EE</label>
        </form>
    </div>

    <div>
        <?php loadAllLevels()?>
    </div>
</div>

<!--This script opens the display book (displayBook.php) page and passes the ID of the book that is clicked on-->
<script type="text/javascript">
    function clickDiv(clicked_id){
        var langID = document.body.id;
        var rules = '';
        var form = document.rules;
        for (var i = 0; i < form.elements.length; i++ ) {
            if (form.elements[i].type == 'checkbox') {
                if (form.elements[i].checked == true) {
                    if (rules==''){
                        rules += form.elements[i].id;
                    }
                    else{
                        rules += ',' + form.elements[i].id;
                    }
                }
            }
        }
        if (rules==''){
            window.location.href="displayBook/displayBook.php?id="+clicked_id+"&lang="+langID;
        }
        else{
            window.location.href="displayBook/displayBook.php?id="+clicked_id+"&lang="+langID+"&rules="+rules;
        }

    }
</script>



</body>
</html>

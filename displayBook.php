<?php

session_start();

require "../actionPages/authorize/checkLogIn.php";


//this connect page makes a DB connection -> creates $mysqli
require "../connectionPages/connect.php";
//this page makes a wordsDB connection -> creates $mysqliRules
require "../connectionPages/connectWords.php";



$bookID = $_GET['id'];
$langID = $_GET['lang'];

//Get the title and frame colour for the clicked book
$SQL = "SELECT book_title, book_colour FROM book WHERE book_id=$bookID";
$bookTitleResults = $mysqli->query($SQL);
$bookTitleRow = mysqli_fetch_array($bookTitleResults);
$bookTitle = $bookTitleRow[0];
$bookColor = "$bookTitleRow[1]";
//replace the spaces with underscores
$bookUnderscore = str_replace(' ','_',$bookTitle);

//
function loadBook($bookID){
    global $mysqli;
    global $mysqliRules;
    global $bookColor;
    global $bookUnderscore;
    global $langID;
    $rules = 0;
    //get the passed values of the checkboxes on the previous page and create the rules array with the results
    if (isset($_GET['rules'])){
        $rules = $_GET['rules'];
    }

    //Get the list of words for the selected rules (* == wordID, word, ruleID, and ruleSearch (the grammar string to search for))
    $ruleWordsSQL = "SELECT * FROM word WHERE ruleID IN ($rules)";
    $wordResults = $mysqliRules->query($ruleWordsSQL);
    global $j;
    $j = 0;
    global $wordsArray;
    global $wordRulesArray;
    //create two empty arrays
    $wordsArray = array();
    $wordRulesArray = array();
    while ($row = mysqli_fetch_row($wordResults)){
        //this creates an array of the words that the rule applies to
        $wordsArray[$j] = $row[1];
        //this creates an array of the 'grammar string' that the rule applies to
        $wordRulesArray[$j] = $row[3];
        $j++;
    }
    //encode for JS use later
    $wordsArray=json_encode($wordsArray);
    $wordRulesArray=json_encode($wordRulesArray);

    //after this is done I have an array of words and an array of rules with matching indexes (ie. array1[0] == array2[0])

    //Run Query for Pages of the book (WHERE bookID) and the language (WHERE langID)
    $pagesSQL = "SELECT * FROM pages WHERE bookID = $bookID AND langID = $langID ORDER BY pageImagePath;";
    $pageResults = $mysqli -> query($pagesSQL);

    //start blank output string
    $output = "";

    $rowPage = mysqli_fetch_row($pageResults);

    //create the swiper slide for the cover separately (it has no text and is full height)
    $output.="<div class='swiper-slide'><img src='../bookManager/$rowPage[1]' class='cover img-responsive'></div>";

    $wordList="";

    //while rows exist in the query results, post the image for that result and the caption into a caption box
    $counter=1;
    while ($rowPage = mysqli_fetch_row($pageResults)){


        $wordList.=$rowPage[2];
        $output .= "<div class='swiper-slide'>
                        <div id='main'>
                            <img src='../bookManager/$rowPage[1]' class='bookpage'>
                            <!-- create the home button and fullscreen button (these are absolutely positioned via css)-->
                            <a href='/index.php' class='close'>
                                <span class='glyphicon glyphicon-home'></span>
                            </a>
                            <a class='fullScreen' onclick='launchFullScreen(document.documentElement)'>
                                <span class='glyphicon glyphicon-fullscreen'></span>
                            </a>
                            <!-- link to the audio that reads this page-->
                            <audio id='page$counter' src='/bookManager/uploadedBooks/$bookUnderscore/audio/page$counter' type='audio/wav'></audio>


                            <div class='captionBox' id='$counter'>
                                <img src='../bookManager/uploadedBooks/Borders/$bookColor.jpg' class='bookpage'>
                                <div class='captionInside'>
                                    <span class='heightCenter'></span><h1 class='caption' id='caption$counter'>$rowPage[2]</h1>
                                    <script>
                                        //this takes the caption for this specific page, splits it at the *s put in during upload (splitting the sentence into separate words)
                                        var caption = $('#caption'+$counter).text().split(/[*]+/),
                                            modText = '';
                                        //use the arrays created earlier
                                        var jWordsArray= $wordsArray;
                                        var jWordRulesArray= $wordRulesArray;
                                        var lang = $langID;
                                        for (var i = 0; i <caption.length; i++){
                                            //remove ALL whitespace and other characters, leaving JUST the word in lower case)
                                            caption[i] = caption[i].replace('\u000A','\u000D');
                                            var word = caption[i];
                                            word = word.replace(/[\.,-\/#!$%\^&\**;?:{}=\-_`~()\"\u000A\u000D]+/g,'');
                                            word = word.trim();
                                            word = word.toLowerCase();

                                            //if lang == english
                                            if (lang==1){
                                                if (jWordsArray.indexOf(word)!= -1){
                                                    //if the word is in the rules array, then apply the 'rule' to it (also applies the 'id' to the word)
                                                    var rule = jWordRulesArray[jWordsArray.indexOf(word)];
                                                    var output = caption[i];
                                                    output = output.replace(/\s\u000A\u000D$/g,'');
                                                    var index = output.indexOf(rule);
                                                    //this splits the word at the 'grammar string' mentioned earlier, and applies the 'phonics' class to that piece and leaves the rest the same!
                                                    output = '<span id=w'+word+'>'+output.slice(0,index)+'<span id=w'+word+' class=phonics>'+output.slice(index, index+rule.length)+'</span>'+output.slice(index+rule.length)+'</span>';
                                                    modText += output;
                                                }
                                                else {
                                                    //if it isn't in the rules list, then it just puts it on the page, and applies it's ID
                                                    var output = caption[i]
                                                    if (i == caption.length-1){
                                                        output = output.replace(/[\s\u000A\u000D$]/g,'');
                                                    }
                                                    modText += '<span id=w'+word+'>' + output + '</span>';
                                                }
                                            }
                                        }
                                        //updates the page with the new grammar styled or not styled words
                                        $('#caption'+$counter).html(modText);
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>";
        $counter++;

    }
    //Create a word list from all the pages text
    $wordList = preg_split("/[\s.!]+/", $wordList);
    //remove duplicates from the list
    $wordList=array_unique($wordList);

    //link to the audio files for each word (after trimming all punctuation and making lowercase)
    //this enables playback of individual words
    $audioOutput="";
    foreach ($wordList as $word){
        if ($word != ""){
            $word = trim($word, "*!.,?\"");
            $word = strtolower($word);
            $audioOutput .= "<audio id='$word' preload='auto' src='/words/$word.wav' type='audio/wav'></audio>";
        }
    }

    //return the output to where the function was called
    echo $output;
    echo $audioOutput;

}
?>
<!--print the actual page out -->
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Reframe Education</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <script language="JavaScript">
            function launchFullScreen(element) {
                if(element.requestFullScreen) {
                    element.requestFullScreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullScreen) {
                    element.webkitRequestFullScreen();
                }
            }
        </script>




        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../bootstrap/css/style.css" rel="stylesheet">
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/scripts.js"></script>

        <link rel="stylesheet" href="../dist/css/swiper.min.css">
        <link rel="stylesheet" href="../stylesheets/style.css">
        <link rel="stylesheet" href="../stylesheets/deviceQueries.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src='../dist/js/swiper.min.js'></script>
        <script>
            //this plays the audio for the page on frame click
            $(window).load(function(e){
                $('div.captionBox').click(function(e){
                    //this if checks if the click is inside the border of the div!
                    if (e.pageY > $(this).offset().top + $(this).outerHeight() - 35 || e.pageY < $(this).offset().top + 35
                        || e.pageX > $(this).offset().left + $(this).outerWidth() - 35 || e.pageX < $(this).offset().left + $('img.bookPage').marginLeft + 35){
                        var $num = this.id;
                        $('#page'+$num).get(0).play();
                    }
                });
            });
        </script>
        <script>
            $(window).load(function(e) {
                $("h1.caption").click(function(h){
                    var word = (h.target);
                    word = word.id;
                    var audio = word.substr(1);
                    $('#'+audio).get(0).play();
                });
            });
        </script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui">
    </head>
    <body class="bookBody">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>


    <script>
        // Wait for window load
        $(window).load(function() {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

        <!--This is the preload Icon-->
        <div class="se-pre-con"></div>
    <!-- create the swiper (slider) container and wrapper) -->
        <div class="swiper-container">

            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php loadBook($bookID);?>
            </div>
        </div>


        <script>
            var displaySwiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                effect: 'slide',
                slidesPerView: 'auto',
                centeredSlides: true,
                grabCursor: true,
                centeredSlides: true
            });
        </script>

    </body>
</html>

<?php
//this function loads the books and sorts them based on their levels and numbers
function bookQuery($rowLevel){
    require "connectionPages/connect.php";

    $book ="SELECT book_id, book_path, book_title FROM book WHERE level_id = {$rowLevel} ORDER BY  level_num ASC, book_num ASC";
    $bookList = $mysqli->query($book);


    if (!$bookList) {
        echo 'Could not run query: ' . $mysqli->error;
        echo $session;
        exit;
    }
    $string = "";
	//then it inserts them as images in a div (bootstrap) with a click function that passes the book id to displayBook.php)
    while ($rowBook = mysqli_fetch_row($bookList)){

        $string .= "<div id= '{$rowBook[0]}' class='col-xs-4 col-md-3 minPadding' onclick= 'clickDiv(id)'>
                        <img class='thumbnail' src='bookManager/$rowBook[1]/coverThumb.jpg' role='button'>
                    </div>";
    
    }
    return $string;
        
}

function loadAllLevels(){
    //connect to mysql db
    require "connectionPages/connect.php";

    $SQL = 	"SELECT level_id, level_desc FROM level";
    
    $levelList = $mysqli->query($SQL);
    if (!$levelList) {
        echo 'Could not run query: ' . $mysqli->error;
        exit;
    }
    echo "<style>
    body {
        background: #fff;
        font-size: 14px;
        color:#000;
        margin: 0;
        padding: 0;
    }
    </style>";
	//this echoes out all of the levels and then fills them with the books for that level
    while ($rowLevel = mysqli_fetch_row($levelList)) {
        $string = bookQuery($rowLevel[0]);
        echo "

        <div class='row'>
            <div class='col-md-12 column'>
                <h4 class='text-center' id='$rowLevel[1]'>
                    {$rowLevel[1]}
                </h4>
            </div>
        </div>
        <div class='row'>
            ". $string . "
        </div>

        ";
    }
}

?>